package mary.academyspeakers;

import android.support.annotation.ColorRes;

public enum Track {
    ALL(R.color.colorPrimary),
    ANDROID(R.color.colorAccent),
    FRONTEND(R.color.colorFront),
    COMMON(R.color.colorCommon);

    @ColorRes
    public final int color;

    Track(int color) {
        this.color = color;
    }
}
