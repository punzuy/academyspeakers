package mary.academyspeakers.Presenter;

import java.util.ArrayList;
import java.util.List;

import mary.academyspeakers.Model.NetworkModule;
import mary.academyspeakers.Model.POJO.Root;
import mary.academyspeakers.Model.POJO.Speakers;
import mary.academyspeakers.Model.POJO.Talks;
import mary.academyspeakers.Model.TalksCard;
import mary.academyspeakers.Model.TalksDao;
import mary.academyspeakers.View.IListFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter {

    private final NetworkModule networkModule;
    private final IListFragment iListFragment;
    private final TalksDao dao;

    private Root root;
    public static final String ALL = "all";
    public static final String ANDROID = "android";
    public static final String FRONTEND = "frontend";
    private static final String COMMON = "common";
    private ArrayList<TalksCard> talksCards;
    private Callback callback = new Callback<Root>() {
        @Override
        public void onResponse(Call<Root> call, Response<Root> response) {
            root = response.body();
            talksCards = createTalksCardsList(root, "all");
            saveDataToDatabase(talksCards);
            updateUIFromDatabase();
        }

        @Override
        public void onFailure(Call<Root> call, Throwable t) {
        }
    };

    public MainPresenter(NetworkModule networkModule, IListFragment iListFragment, TalksDao dao) {
        this.networkModule = networkModule;
        this.iListFragment = iListFragment;
        this.dao = dao;
        getData();
    }

    private void getData() {
        iListFragment.showSpinner();
        Call<Root> data = networkModule.getData();
        data.enqueue(callback);
    }

    public void onClickedAll() {
        talksCards = createTalksCardsList(root, ALL);
        iListFragment.showTalks(talksCards);
    }

    public void onClickedAndroid() {
        talksCards = createTalksCardsList(root, ANDROID);
        iListFragment.showTalks(talksCards);
    }

    public void onClickedFront() {
        talksCards = createTalksCardsList(root, FRONTEND);
        iListFragment.showTalks(talksCards);
    }

    public void onClickedCommon() {
        talksCards = createTalksCardsList(root, COMMON);
        iListFragment.showTalks(talksCards);
    }

    private void saveDataToDatabase(List<TalksCard> talksCards) {
        dao.deleteAll();
        dao.insertAll(talksCards.toArray(new TalksCard[talksCards.size()]));
    }

    private Speakers getSpeakerForId(String id) {
        ArrayList<Speakers> speakers = new ArrayList<>(root.getSpeakers());
        Speakers speaker = new Speakers();
        for (Speakers s : speakers) {
            if (s.getId().equals(id)) {
                speaker = s;
            }
        }
        return speaker;
    }

    private ArrayList<TalksCard> createTalksCardsList(Root root, String track) {
        List<Talks> talks = root.getSchedule().getTalks();
        ArrayList<TalksCard> talksCardsList = new ArrayList<>();

        for (Talks talk : talks) {
            if (track.equals("all") || track.equals(talk.getTrack())) {
                TalksCard talksCard = new TalksCard();
                talksCard.setTrack(talk.getTrack());
                talksCard.setTime(talk.getTime());
                Speakers speaker = getSpeakerForId(talk.getSpeaker());
                talksCard.setSpeakerName(speaker.getFirstName().concat(" ").concat(speaker.getLastName()));
                talksCard.setSpeakerAbout(speaker.getAbout());
                talksCard.setTitle(talk.getTitle());
                talksCard.setRoom(talk.getRoom());
                talksCard.setDescription(talk.getDescription());
                talksCardsList.add(talksCard);
            }
        }
        return talksCardsList;
    }

    private void updateUIFromDatabase() {
        iListFragment.showTalks(dao.getAll());
    }
}
