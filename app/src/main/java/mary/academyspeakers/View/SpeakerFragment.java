package mary.academyspeakers.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mary.academyspeakers.Model.TalksCard;
import mary.academyspeakers.Model.TalksDatabase;
import mary.academyspeakers.MyApplication;
import mary.academyspeakers.R;

public class SpeakerFragment extends Fragment {

    private static final String SPEAKER_KEY = "speakerkey";

    private String talkTitle;

    public SpeakerFragment() {
        // Required empty public constructor
    }

    public static SpeakerFragment newInstance(String title) {
        SpeakerFragment fragment = new SpeakerFragment();
        Bundle args = new Bundle();
        args.putString(SPEAKER_KEY, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            talkTitle = getArguments().getString(SPEAKER_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_speaker, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        TextView name = view.findViewById(R.id.fragment_speaker_title);
        TextView description = view.findViewById(R.id.fragment_speaker_description);

        TalksDatabase db = MyApplication.getInstance().getDatabase();
        TalksCard talk = db.talksDao().getTalkForTitle(talkTitle);

        name.setText(talk.getSpeakerName());
        description.setText(talk.getSpeakerAbout());
    }
}

