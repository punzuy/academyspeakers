package mary.academyspeakers.View;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import mary.academyspeakers.Model.TalksCard;
import mary.academyspeakers.R;
import mary.academyspeakers.Track;

public class TalksAdapter extends RecyclerView.Adapter<TalksAdapter.ViewHolder> {

    private OnFragmentInteractionListener listener;
    private List<TalksCard> talks;

    TalksAdapter(OnFragmentInteractionListener listener, List<TalksCard> talks) {
        this.listener = listener;
        this.talks = talks;
    }

    @NonNull
    @Override
    public TalksAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.talk_card, parent, false);
        return new TalksAdapter.ViewHolder(listener, v, talks);
    }

    @Override
    public void onBindViewHolder(@NonNull TalksAdapter.ViewHolder holder, int position) {
        final TalksCard talk = talks.get(position);

        holder.name.setText(talk.getSpeakerName());
        holder.time.setText(talk.getTime());
        holder.title.setText(talk.getTitle());
        holder.room.setText("Room " + String.valueOf(talk.getRoom()));
        holder.track.setText(talk.getTrack());
        holder.track.setBackgroundColor(ContextCompat.getColor(
                holder.track.getContext(),
                Track.valueOf(talk.getTrack().toUpperCase()).color
        ));
    }

    @Override
    public int getItemCount() {
        return talks.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView time;
        TextView room;
        TextView title;
        TextView name;
        TextView track;

        ViewHolder(final OnFragmentInteractionListener listener, View v, final List<TalksCard> talks) {
            super(v);
            time = v.findViewById(R.id.time);
            room = v.findViewById(R.id.room);
            title = v.findViewById(R.id.title);
            name = v.findViewById(R.id.name);
            track = v.findViewById(R.id.track);
            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        String title = talks.get(getAdapterPosition()).getTitle();
                        listener.onTalkSelected(title);
                    }
                }
            });
            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        String title = talks.get(getAdapterPosition()).getTitle();
                        listener.onSpeakerSelected(title);
                    }
                }
            });
        }
    }
}

