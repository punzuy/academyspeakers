package mary.academyspeakers.View;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import mary.academyspeakers.Model.NetworkModule;
import mary.academyspeakers.Model.TalksCard;
import mary.academyspeakers.MyApplication;
import mary.academyspeakers.Presenter.MainPresenter;
import mary.academyspeakers.R;

public class ListFragment extends Fragment implements IListFragment, View.OnClickListener {

    private MainPresenter mainPresenter;
    private ProgressBar spinner;
    private TextView noInternet;
    private RecyclerView usersRecycler;
    private OnFragmentInteractionListener listener;

    public ListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        usersRecycler = view.findViewById(R.id.talksRecycler);
        spinner = view.findViewById(R.id.spinner);
        noInternet = view.findViewById(R.id.noInternet);

        Button all = view.findViewById(R.id.all);
        Button android = view.findViewById(R.id.android);
        Button front = view.findViewById(R.id.front);
        Button common = view.findViewById(R.id.common);

        all.setOnClickListener(this);
        android.setOnClickListener(this);
        front.setOnClickListener(this);
        common.setOnClickListener(this);

        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            noInternet.setVisibility(View.GONE);
            mainPresenter = new MainPresenter(new NetworkModule(), this, MyApplication.getInstance().getDatabase().talksDao());

        } else {
            showNoInternetMessage();
        }
    }

    public void showNoInternetMessage() {
        hideSpinner();
        noInternet.setVisibility(View.VISIBLE);
    }

    public void hideSpinner() {
        spinner.setVisibility(View.GONE);
    }

    @Override
    public void showTalks(List<TalksCard> talks) {
        hideSpinner();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        usersRecycler.setLayoutManager(linearLayoutManager);
        usersRecycler.setAdapter(new TalksAdapter(listener, talks));
    }

    @Override
    public void showSpinner() {
        spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.all: {
                mainPresenter.onClickedAll();
                break;
            }
            case R.id.android: {
                mainPresenter.onClickedAndroid();
                break;
            }
            case R.id.front: {
                mainPresenter.onClickedFront();
                break;
            }
            case R.id.common: {
                mainPresenter.onClickedCommon();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
