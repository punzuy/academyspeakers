package mary.academyspeakers.View;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import mary.academyspeakers.R;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListFragment listFragment = new ListFragment();

        getSupportFragmentManager()
                .beginTransaction()

                .add(R.id.activity_main_frame, listFragment)
                .commit();
    }

    public void onTalkSelected(String title) {
        TalkFragment talkFragment = TalkFragment.newInstance(title);

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.activity_main_frame, talkFragment)
                .commit();
    }

    @Override
    public void onSpeakerSelected(String title) {
        SpeakerFragment speakerFragment = SpeakerFragment.newInstance(title);

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.activity_main_frame, speakerFragment)
                .commit();
    }
}
