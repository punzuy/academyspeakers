package mary.academyspeakers.View;

public interface OnFragmentInteractionListener {

    void onTalkSelected(String title);

    void onSpeakerSelected(String title);
}
