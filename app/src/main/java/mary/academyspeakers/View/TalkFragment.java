package mary.academyspeakers.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mary.academyspeakers.Model.TalksCard;
import mary.academyspeakers.Model.TalksDatabase;
import mary.academyspeakers.MyApplication;
import mary.academyspeakers.R;

public class TalkFragment extends Fragment {

    private static final String TALK_KEY = "talkkey";

    private String talkTitle;

    public TalkFragment() {
        // Required empty public constructor
    }

    public static TalkFragment newInstance(String title) {
        TalkFragment fragment = new TalkFragment();
        Bundle args = new Bundle();
        args.putString(TALK_KEY, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            talkTitle = getArguments().getString(TALK_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_talk, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        TextView title = view.findViewById(R.id.fragment_talk_title);
        TextView description = view.findViewById(R.id.fragment_talk_description);

        TalksDatabase db = MyApplication.getInstance().getDatabase();
        TalksCard talk = db.talksDao().getTalkForTitle(talkTitle);

        title.setText(talkTitle);
        description.setText(talk.getDescription());
    }
}

