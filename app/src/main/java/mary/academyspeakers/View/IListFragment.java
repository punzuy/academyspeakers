package mary.academyspeakers.View;

import java.util.List;

import mary.academyspeakers.Model.TalksCard;

public interface IListFragment {

    void showTalks(List<TalksCard> talks);

    void showSpinner();
}
