package mary.academyspeakers.Model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface TalksDao {

    @Query("SELECT * FROM talkscard")
    List<TalksCard> getAll();

    @Insert
    void insertAll(TalksCard... talksCards);

    @Delete
    void delete(TalksCard talksCard);

    @Query("DELETE FROM talkscard")
    void deleteAll();

    @Query("SELECT * FROM talkscard WHERE title = :titleParam LIMIT 1")
    TalksCard getTalkForTitle(String titleParam);

}
