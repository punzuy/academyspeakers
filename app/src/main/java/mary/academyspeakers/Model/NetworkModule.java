package mary.academyspeakers.Model;

import mary.academyspeakers.Model.POJO.Root;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkModule {

    private static final String BASE_URL = "https://storage.yandexcloud.net/devfestapi/";

    public Call<Root> getData() {
        Retrofit.Builder retrofitBuilder =
                new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = retrofitBuilder.build();
        APIService APIService = retrofit.create(APIService.class);
        return APIService.getData();
    }
}

