package mary.academyspeakers.Model;

import mary.academyspeakers.Model.POJO.Root;
import retrofit2.Call;
import retrofit2.http.GET;

public interface APIService {

    @GET("https://storage.yandexcloud.net/devfestapi/data.json")
    Call<Root> getData();
}
