package mary.academyspeakers.Model;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = TalksCard.class, version = 1)
public abstract class TalksDatabase extends RoomDatabase {
    public abstract TalksDao talksDao();
}
