package mary.academyspeakers.Model.POJO;

import java.util.List;

public class Root {

    private List<Speakers> speakers;
    private Schedule schedule;

    public List<Speakers> getSpeakers() {
        return this.speakers;
    }

    public void setSpeakers(List<Speakers> speakers) {
        this.speakers = speakers;
    }

    public Schedule getSchedule() {
        return this.schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }
}

