package mary.academyspeakers.Model.POJO;

import java.util.List;

public class Schedule {

    private List<Talks> talks;
    private List<Activities> activities;

    public List<Talks> getTalks() {
        return this.talks;
    }

    public void setTalks(List<Talks> talks) {
        this.talks = talks;
    }

    public List<Activities> getActivities() {
        return this.activities;
    }

    public void setActivities(List<Activities> activities) {
        this.activities = activities;
    }
}

