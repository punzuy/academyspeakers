package mary.academyspeakers;

import android.app.Application;
import android.arch.persistence.room.Room;

import mary.academyspeakers.Model.TalksDatabase;

public class MyApplication extends Application {

    private static final String DATABASE_NAME = "TalksDb.db";
    public static MyApplication instance;
    private TalksDatabase database;

    public static MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(this, TalksDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
    }

    public TalksDatabase getDatabase() {
        return database;
    }
}